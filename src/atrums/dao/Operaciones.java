package atrums.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import atrums.persistencia.Conexion;
import atrums.persistencia.OperacionesBDD;
import javax.sql.DataSource;

@Path("/")
public class Operaciones {
	static final Logger log = Logger.getLogger(Operaciones.class);
	private Conexion dataconexion = new Conexion();
	private DataSource dataSourceOpenbravo = null;
	
	public Operaciones() {
		if(this.dataSourceOpenbravo == null){
			this.dataSourceOpenbravo = dataconexion.crearConexionPostgresOpenbravo();
		}
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public void servicio(@Context HttpServletResponse servletResponse){
		try {
			servletResponse.sendRedirect("../index.html");
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
	}
	
	@GET
	@Path("pedido")
	public void pedidoOperacion(@Context HttpServletResponse servletResponse) throws IOException{
		try {
			servletResponse.sendRedirect("../pedido.html");
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
	}
	
	@PUT
	@Path("pedidoput")
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String GuardarPedido(
			@FormParam("usuario") String usuario, 
			@FormParam("password") String password, 
			@FormParam("sucursal") String sucursal, 
			@FormParam("nrodocumento") String nrodocumento, 
			@FormParam("identificacion") String identificacion, 
			@FormParam("fecha") String fecha) {
		return guardarPedidoVenta(usuario, password, sucursal, nrodocumento, identificacion, fecha);
	}
	
	@POST
	@Path("pedidopost")
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String guardarPedidoPostForm(
			@FormParam("usuario") String usuario, 
			@FormParam("password") String password, 
			@FormParam("sucursal") String sucursal, 
			@FormParam("nrodocumento") String nrodocumento, 
			@FormParam("identificacion") String identificacion, 
			@FormParam("fecha") String fecha) {
		return guardarPedidoVenta(usuario, password, sucursal, nrodocumento, identificacion, fecha);
	}
	
	@POST
	@Path("pedidopostjson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String guardarPedidoPostJson(String inputJsonObj){
		String usuario = "";
		String password = "";
		String sucursal = "";
		String nrodocumento = "";
		String identificacion = "";
		String fecha = "";
		
		//String mensaje = "<?xml version=\"1.0\"?>";
		
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);			
			
			usuario = (String) auxInputJsonObj.get("usuario");
			password = (String) auxInputJsonObj.get("password");
			sucursal = (String) auxInputJsonObj.get("sucursal");
			nrodocumento = (String) auxInputJsonObj.get("nrodocumento");
			identificacion = (String) auxInputJsonObj.get("identificacion");
			fecha = (String) auxInputJsonObj.get("fecha");
			
			//mensaje = guardarPedidoVenta(usuario, password, sucursal, nrodocumento, identificacion, fecha);
			auxmensaje = guardarPedidoVentaJson(usuario, password, sucursal, nrodocumento, identificacion, fecha);
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			//mensaje = mensaje + "<warn>" + ex.getMessage() + "</warn>";
			try {
				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	@PUT
	@Path("pedidoputjson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String guardarPedidoPutJson(String inputJsonObj){
		String usuario = "";
		String password = "";
		String sucursal = "";
		String nrodocumento = "";
		String identificacion = "";
		String fecha = "";
		
		//String mensaje = "<?xml version=\"1.0\"?>";
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);			
			
			usuario = (String) auxInputJsonObj.get("usuario");
			password = (String) auxInputJsonObj.get("password");
			sucursal = (String) auxInputJsonObj.get("sucursal");
			nrodocumento = (String) auxInputJsonObj.get("nrodocumento");
			identificacion = (String) auxInputJsonObj.get("identificacion");
			fecha = (String) auxInputJsonObj.get("fecha");
			
			//mensaje = guardarPedidoVenta(usuario, password, sucursal, nrodocumento, identificacion, fecha);
			auxmensaje = guardarPedidoVentaJson(usuario, password, sucursal, nrodocumento, identificacion, fecha);
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			//mensaje = mensaje + "<warn>" + ex.getMessage() + "</warn>";
			try {
				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	@GET
	@Path("lineapedido")
	public void lineaPedidoOperacion(@Context HttpServletResponse servletResponse) throws IOException{
		try {
			servletResponse.sendRedirect("../lineapedido.html");
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
	}
	
	@POST
	@Path("lineapedidopost")
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String guardarLineaPedidoPostForm(
			@FormParam("usuario") String usuario, 
			@FormParam("password") String password, 
			@FormParam("sucursal") String sucursal, 
			@FormParam("nrodocumento") String nrodocumento, 
			@FormParam("producto") String producto, 
			@FormParam("cantidad") String cantidad, 
			@FormParam("preciounitario") String preciounitario, 
			@FormParam("impuesto") String impuesto) {
		return guardarLineaPedidoVenta(usuario, password, sucursal, nrodocumento, producto, cantidad, preciounitario, impuesto);
	}
	
	@PUT
	@Path("lineapedidoput")
	@Produces(MediaType.APPLICATION_XML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String guardarLineaPedidoPutForm(
			@FormParam("usuario") String usuario, 
			@FormParam("password") String password, 
			@FormParam("sucursal") String sucursal, 
			@FormParam("nrodocumento") String nrodocumento, 
			@FormParam("producto") String producto, 
			@FormParam("cantidad") String cantidad, 
			@FormParam("preciounitario") String preciounitario, 
			@FormParam("impuesto") String impuesto) {
		return guardarLineaPedidoVenta(usuario, password, sucursal, nrodocumento, producto, cantidad, preciounitario, impuesto);
	}
	
	@POST
	@Path("lineapedidopostjson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String guardarLineaPedidoPostJson(String inputJsonObj) {
		
		String usuario = "";
		String password = "";
		String sucursal = "";
		String nrodocumento = "";
		String producto = "";
		String cantidad = "";
		String preciounitario = "";
		String impuesto = "";
		
		//String mensaje = "<?xml version=\"1.0\"?>";
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);
			
			usuario = (String) auxInputJsonObj.get("usuario");
			password = (String) auxInputJsonObj.get("password");
			sucursal = (String) auxInputJsonObj.get("sucursal");
			nrodocumento = (String) auxInputJsonObj.get("nrodocumento");
			producto = (String) auxInputJsonObj.get("producto");
			cantidad = (String) auxInputJsonObj.get("cantidad");
			preciounitario = (String) auxInputJsonObj.get("preciounitario");
			impuesto = (String) auxInputJsonObj.get("impuesto");
			
			//return guardarLineaPedidoVenta(usuario, password, sucursal, nrodocumento, producto, cantidad, preciounitario, impuesto);
			auxmensaje = guardarLineaPedidoVentaJson(usuario, password, sucursal, nrodocumento, producto, cantidad, preciounitario, impuesto);
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			//mensaje = mensaje + "<warn>" + ex.getMessage() + "</warn>";
			try {
				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	@PUT
	@Path("lineapedidoputjson")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String guardarLineaPedidoPutJson(String inputJsonObj) {
		
		String usuario = "";
		String password = "";
		String sucursal = "";
		String nrodocumento = "";
		String producto = "";
		String cantidad = "";
		String preciounitario = "";
		String impuesto = "";
		
		//String mensaje = "<?xml version=\"1.0\"?>";
		JSONObject auxmensaje = new JSONObject();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);
			
			usuario = (String) auxInputJsonObj.get("usuario");
			password = (String) auxInputJsonObj.get("password");
			sucursal = (String) auxInputJsonObj.get("sucursal");
			nrodocumento = (String) auxInputJsonObj.get("nrodocumento");
			producto = (String) auxInputJsonObj.get("producto");
			cantidad = (String) auxInputJsonObj.get("cantidad");
			preciounitario = (String) auxInputJsonObj.get("preciounitario");
			impuesto = (String) auxInputJsonObj.get("impuesto");
			
			//return guardarLineaPedidoVenta(usuario, password, sucursal, nrodocumento, producto, cantidad, preciounitario, impuesto);
			auxmensaje = guardarLineaPedidoVentaJson(usuario, password, sucursal, nrodocumento, producto, cantidad, preciounitario, impuesto);
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			//mensaje = mensaje + "<warn>" + ex.getMessage() + "</warn>";
			try {
				auxmensaje.put("warn", ex.getMessage());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return auxmensaje.toString();
	}
	
	private String guardarPedidoVenta(String usuario, 
			String password, 
			String sucursal, 
			String nrodocumento, 
			String identificacion, 
			String fecha){
		
		boolean continuar = true;
		
		String respuesta = "<?xml version=\"1.0\"?>" 
				+ "<documento>" 
				+ "<accesoseguridad>"; 
		
		if(usuario.equals("")){
			respuesta = respuesta + "<usuario>No hay usuario</usuario>";
			continuar = false;
		}else{
			respuesta = respuesta + "<usuario>" + usuario + "</usuario>";
		}
		
		if(password.equals("")){
			respuesta = respuesta + "<password>No hay password</password>";
			continuar = false;
		}else{
			respuesta = respuesta + "<password>*********</password>";
		}
		
		respuesta = respuesta + "</accesoseguridad>" + "<pedido>";
		
		if(sucursal.equals("")){
			respuesta = respuesta + "<sucursal>No hay sucursal</sucursal>";
			continuar = false;
		}else{
			respuesta = respuesta + "<sucursal>" + sucursal + "</sucursal>";
		}
		
		if(nrodocumento.equals("")){
			respuesta = respuesta + "<nrodocumento>No hay nro. documento</nrodocumento>";
			continuar = false;
		}else{
			respuesta = respuesta + "<nrodocumento>" + nrodocumento + "</nrodocumento>";
		}
		
		if(identificacion.equals("")){
			respuesta = respuesta + "<identificacion>No hay un identificacion</identificacion>";
			continuar = false;
		}else{
			respuesta = respuesta + "<identificacion>" + identificacion + "</identificacion>";
		}
		
		if(fecha.equals("")){
			respuesta = respuesta + "<fecha>No hay fecha</fecha>";
			continuar = false;
		}else{
			if(fecha.indexOf("/") != -1 || fecha.indexOf("-") != -1){
				respuesta = respuesta + "<fecha>" + fecha + "</fecha>";
			}else{
				respuesta = respuesta + "<fecha>Formato de fecha equivocado, correcto Dia/Mes/A�o o Dia-Mes-A�o</fecha>";
				continuar = false;
			}
			
		}
		
		respuesta = respuesta + "</pedido>";
		
		//Inicio de operaciones
		respuesta = respuesta + "<respuesta>";
		
		if(continuar){
			Connection conHome = null;
			
			try{
				OperacionesBDD home = new OperacionesBDD();
				conHome = home.getConneccion(dataSourceOpenbravo);
				
				if(conHome == null){
					respuesta = respuesta + "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador";
				}else if(!home.loggin(usuario, password, conHome)){
					respuesta = respuesta + "Usuario o Password Incorrectos";
				}else{
					respuesta = respuesta + home.savePedido(sucursal, nrodocumento, identificacion, fecha, conHome);
				}
			}catch (Exception ex) {
				// TODO: handle exception
				log.warn(ex.getMessage());
			}finally {
				try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
			}
		}else{
			respuesta = respuesta + "Verifique los datos";
		}
		
		respuesta = respuesta + "</respuesta>";
		//Fin de operaciones
		
		respuesta = respuesta + "</documento>";
				
		return respuesta;
	}
	
	private JSONObject guardarPedidoVentaJson(String usuario, 
			String password, 
			String sucursal, 
			String nrodocumento, 
			String identificacion, 
			String fecha){
		
		boolean continuar = true;
		
		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();
		
		try{
			if(usuario.equals("")){
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			}else{
				auxSeguridad.put("usuario", usuario);
			}
			
			if(password.equals("")){
				auxSeguridad.put("password", "No hay password");
				continuar = false;
			}else{
				auxSeguridad.put("password", "*********");
			}
			
			auxRespuesta.put("accesoseguridad", auxSeguridad);
			JSONObject auxPedido = new JSONObject();
			
			if(sucursal.equals("")){
				auxPedido.put("sucursal", "No hay sucursal");
				continuar = false;
			}else{
				auxPedido.put("sucursal", sucursal);
			}
			
			if(nrodocumento.equals("")){
				auxPedido.put("nrodocumento", "No hay nro. documento");
				continuar = false;
			}else{
				auxPedido.put("nrodocumento", nrodocumento);
			}
			
			if(identificacion.equals("")){
				auxPedido.put("identificacion", "No hay un identificacion");
				continuar = false;
			}else{
				auxPedido.put("identificacion", identificacion);
			}
			
			if(fecha.equals("")){
				auxPedido.put("fecha", "No hay fecha");
				continuar = false;
			}else{
				if(fecha.indexOf("/") != -1 || fecha.indexOf("-") != -1){
					auxPedido.put("fecha", fecha);
				}else{
					auxPedido.put("fecha", "Formato de fecha equivocado, correcto Dia/Mes/A�o o Dia-Mes-A�o");
					continuar = false;
				}
				
			}
			
			auxRespuesta.put("pedido", auxPedido);
			
			if(continuar){
				Connection conHome = null;
				
				try{
					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);
					
					if(conHome == null){
						auxRespuesta.put("respuesta", "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
					}else if(!home.loggin(usuario, password, conHome)){
						auxRespuesta.put("respuesta", "Usuario o Password Incorrectos");
					}else{
						auxRespuesta.put("respuesta", home.savePedido(sucursal, nrodocumento, identificacion, fecha, conHome));
					}
				}catch (Exception ex) {
					log.warn(ex.getMessage());
				}finally {
					try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
				}
			}else{
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}
		
		return auxRespuesta;
	}
	
	private String guardarLineaPedidoVenta(
			String usuario, 
			String password, 
			String sucursal, 
			String nrodocumento, 
			String producto, 
			String cantidad, 
			String preciounitario, 
			String impuesto){
		
		boolean continuar = true;
		int auxCantidad = 0;
		Float auxpreciounitario = (float) 0;
		
		String respuesta = "<?xml version=\"1.0\"?>" 
				+ "<documento>" 
				+ "<accesoseguridad>"; 
		
		if(usuario.equals("")){
			respuesta = respuesta + "<usuario>No hay usuario</usuario>";
			continuar = false;
		}else{
			respuesta = respuesta + "<usuario>" + usuario + "</usuario>";
		}
		
		if(password.equals("")){
			respuesta = respuesta + "<password>No hay password</password>";
			continuar = false;
		}else{
			respuesta = respuesta + "<password>*********</password>";
		}
		
		respuesta = respuesta + "</accesoseguridad>" + "<lineapedido>";
		
		if(sucursal.equals("")){
			respuesta = respuesta + "<sucursal>No hay sucursal</sucursal>";
			continuar = false;
		}else{
			respuesta = respuesta + "<sucursal>" + sucursal + "</sucursal>";
		}
		
		if(nrodocumento.equals("")){
			respuesta = respuesta + "<nrodocumento>No hay nro. documento</nrodocumento>";
			continuar = false;
		}else{
			respuesta = respuesta + "<nrodocumento>" + nrodocumento + "</nrodocumento>";
		}
		
		if(producto.equals("")){
			respuesta = respuesta + "<producto>No hay producto</producto>";
			continuar = false;
		}else{
			respuesta = respuesta + "<producto>" + producto + "</producto>";
		}
		
		if(cantidad.equals("")){
			respuesta = respuesta + "<cantidad>No hay cantidad</cantidad>";
			continuar = false;
		}else{
			try{
				auxCantidad = Integer.valueOf(cantidad);
				respuesta = respuesta + "<cantidad>" + cantidad + "</cantidad>";
			}catch (Exception ex) {
				// TODO: handle exception
				respuesta = respuesta + "<cantidad>La cantidad no es un n�mero</cantidad>";
				continuar = false;
			}
		}
		
		if(preciounitario.equals("")){
			respuesta = respuesta + "<preciounitario>No hay precio unitario</preciounitario>";
			continuar = false;
		}else{
			try {
				preciounitario = preciounitario.replaceAll(",", ".");
				auxpreciounitario = Float.valueOf(preciounitario);
				respuesta = respuesta + "<preciounitario>" + preciounitario + "</preciounitario>";
			} catch (Exception ex) {
				// TODO: handle exception
				respuesta = respuesta + "<preciounitario>EL Precio Unitario no es un n�mero o el formato esta incorrecto, correcto 00.00</preciounitario>";
				continuar = false;
			}
		}
		
		if(impuesto.equals("")){
			respuesta = respuesta + "<impuesto>No hay impuesto</impuesto>";
			continuar = false;
		}else{
			respuesta = respuesta + "<impuesto>" + impuesto + "</impuesto>";
		}
		
		respuesta = respuesta + "</lineapedido>";
		
		//Inicio de operaciones
		respuesta = respuesta + "<respuesta>";
		
		if(continuar){
			Connection conHome = null;
			
			try{
				OperacionesBDD home = new OperacionesBDD();
				conHome = home.getConneccion(dataSourceOpenbravo);
				
				if(conHome == null){
					respuesta = respuesta + "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador";
				}else if(!home.loggin(usuario, password, conHome)){
					respuesta = respuesta + "Usuario o Password Incorrectos";
				}else{
					respuesta = respuesta + home.savePedidoLinea(sucursal, nrodocumento, producto, auxCantidad, auxpreciounitario, impuesto, conHome);
				}
			}catch (Exception ex) {
				// TODO: handle exception
				log.warn(ex.getMessage());
			}finally {
				try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
			}
		}else{
			respuesta = respuesta + "Verifique los datos";
		}
		
		respuesta = respuesta + "</respuesta>";
		//Fin de operaciones
		
		respuesta = respuesta + "</documento>";
				
		return respuesta;
	}
	
	private JSONObject guardarLineaPedidoVentaJson(
			String usuario, 
			String password, 
			String sucursal, 
			String nrodocumento, 
			String producto, 
			String cantidad, 
			String preciounitario, 
			String impuesto){
		
		boolean continuar = true;
		int auxCantidad = 0;
		Float auxpreciounitario = (float) 0;
		
		JSONObject auxRespuesta = new JSONObject();
		JSONObject auxSeguridad = new JSONObject();
		
		try{
			if(usuario.equals("")){
				auxSeguridad.put("usuario", "No hay usuario");
				continuar = false;
			}else{
				auxSeguridad.put("usuario", usuario);
			}
			
			if(password.equals("")){
				auxSeguridad.put("password", "No hay password");
				continuar = false;
			}else{
				auxSeguridad.put("password", "*********");
			}
			auxRespuesta.put("accesoseguridad", auxSeguridad);
			JSONObject auxLinea = new JSONObject();
			
			if(sucursal.equals("")){
				auxLinea.put("sucursal", "No hay sucursal");
				continuar = false;
			}else{
				auxLinea.put("sucursal", sucursal);
			}
			
			if(nrodocumento.equals("")){
				auxLinea.put("nrodocumento", "No hay nro. documento");
				continuar = false;
			}else{
				auxLinea.put("nrodocumento", nrodocumento);
			}
			
			if(producto.equals("")){
				auxLinea.put("producto", "No hay producto");
				continuar = false;
			}else{
				auxLinea.put("producto", producto);
			}
			
			if(cantidad.equals("")){
				auxLinea.put("cantidad", "No hay cantidad");
				continuar = false;
			}else{
				try{
					auxCantidad = Integer.valueOf(cantidad);
					auxLinea.put("cantidad", cantidad);
				}catch (Exception ex) {
					// TODO: handle exception
					auxLinea.put("cantidad", "<cantidad>La cantidad no es un n�mero</cantidad>");
					continuar = false;
				}
			}
			
			if(preciounitario.equals("")){
				auxLinea.put("preciounitario", "No hay precio unitario");
				continuar = false;
			}else{
				try {
					preciounitario = preciounitario.replaceAll(",", ".");
					auxpreciounitario = Float.valueOf(preciounitario);
					auxLinea.put("preciounitario", preciounitario);
				} catch (Exception ex) {
					// TODO: handle exception
					auxLinea.put("preciounitario", "<preciounitario>EL Precio Unitario no es un n�mero o el formato esta incorrecto, correcto 00.00</preciounitario>");
					continuar = false;
				}
			}
			
			if(impuesto.equals("")){
				auxLinea.put("impuesto", "No hay impuesto");
				continuar = false;
			}else{
				auxLinea.put("impuesto", impuesto);
			}
			
			auxRespuesta.put("lineapedido", auxLinea);
			
			if(continuar){
				Connection conHome = null;
				
				try{
					OperacionesBDD home = new OperacionesBDD();
					conHome = home.getConneccion(dataSourceOpenbravo);
					
					if(conHome == null){
						auxRespuesta.put("respuesta", "No hay conexi�n con la BDD de Openbravo, comun�quese con el administrador");
					}else if(!home.loggin(usuario, password, conHome)){
						auxRespuesta.put("respuesta", "Usuario o Password Incorrectos");
					}else{
						auxRespuesta.put("respuesta", home.savePedidoLinea(sucursal, nrodocumento, producto, auxCantidad, auxpreciounitario, impuesto, conHome));
					}
				}catch (Exception ex) {
					// TODO: handle exception
					log.warn(ex.getMessage());
				}finally {
					try {if(conHome != null)conHome.close();} catch (SQLException ex) {}
				}
			}else{
				auxRespuesta.put("respuesta", "Verifique los datos");
			}
		} catch (JSONException e) {
			log.warn(e.getMessage());
		}
			
		return auxRespuesta;
	}
}
