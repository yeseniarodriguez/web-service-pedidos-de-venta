package atrums.persistencia;

import java.security.MessageDigest;
import org.apache.commons.codec.binary.Base64;

public class OperacionesAuxiliares {
	
	public String encryptSh1(String x) throws Exception {
		MessageDigest d = null;
        d = MessageDigest.getInstance("SHA-1");
        d.reset();
        d.update(x.getBytes());
        
        byte[] bytesen = Base64.encodeBase64(d.digest());
        return new String(bytesen, "UTF-8");
	}
}
