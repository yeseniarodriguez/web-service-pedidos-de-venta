package atrums.persistencia;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import atrums.modelo.Configuracion;

public class OperacionesBDD {
	static final Logger log = Logger.getLogger(OperacionesBDD.class);
	private Configuracion configuracion = new Configuracion();
	
	public OperacionesBDD() {
		super();
	}
	
	public Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		Connection connection = null;
		
		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);
			
			if(connection.isClosed()){
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
		
		return connection;
	}
	
	public boolean loggin(String usuario, String password, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		boolean loggin = false;
		
		try {
			ResultSet rs = null;
			statement = connection.createStatement();
			
			OperacionesAuxiliares auxiliares = new OperacionesAuxiliares();
			password = auxiliares.encryptSh1(password);
			
			sql = "SELECT count(*) as total "
					+ "FROM ad_user ad "
					+ "WHERE ad.username = '" + usuario + "' AND ad.password = '" + password + "' AND ad.isactive = 'Y';";
			rs = statement.executeQuery(sql);
			
			while (rs.next()){
				if(rs.getInt("total") == 1){
					loggin = true;
				}
			}
			
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return loggin;
	}
	
	public String savePedido(String sucursal, 
			String nrodocumento,
			String tercero,
			String fecha,
			Connection connection) {
		// TODO Auto-generated method stub
		ResultSet rs = null;
		Statement statement = null;
		String sql;
		
		int registro = 0;
		String cliente = null;
		String organizacion = null;
		String doctipe = null;
		String cbpartner = null;
		String cbpartnerdir = null;
		String bodega = null;
		String paymentterm = null;
		String pricelist = null;
		String metodopago = null;
		boolean existedoc = false; 
		
		String mensaje = "";
		
		try {
			statement = connection.createStatement();
			
			rs = null;
			sql = "SELECT ad_org_id, ad_client_id FROM ad_org WHERE upper(value) LIKE '%" + sucursal.toUpperCase() + "%' LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				organizacion = rs.getString("ad_org_id");
				cliente = rs.getString("ad_client_id");
			}
			
			rs = null;
			sql = "SELECT c_doctype_id FROM c_doctype WHERE ad_client_id = '" + cliente + "' AND upper(name) LIKE '%" + this.configuracion.getDocpedido().toUpperCase() + "%';";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				doctipe = rs.getString("c_doctype_id");
			}
			
			rs = null;
			sql = "SELECT c_bpartner_id FROM c_bpartner WHERE ad_client_id = '" + cliente + "' AND value = '" + tercero + "';";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				cbpartner = rs.getString("c_bpartner_id");
			}
			
			rs = null;
			sql = "SELECT c_bpartner_location_id FROM c_bpartner_location WHERE c_bpartner_id = '" + cbpartner + "' AND isactive = 'Y' LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				cbpartnerdir = rs.getString("c_bpartner_location_id");
			}
			
			rs = null;
			sql = "SELECT c_paymentterm_id FROM c_paymentterm WHERE ad_client_id = '" + cliente + "' AND upper(name) LIKE '%" + this.configuracion.getPagoterm().toUpperCase() + "%' AND isactive = 'Y' LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				paymentterm = rs.getString("c_paymentterm_id");
			}
			
			rs = null;
			sql = "SELECT m_warehouse_id FROM ad_org_warehouse WHERE ad_org_id = '" + organizacion + "' AND isactive = 'Y' LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				bodega = rs.getString("m_warehouse_id");
			}
			
			rs = null;
			sql = "SELECT m_pricelist_id FROM m_pricelist WHERE ad_client_id = '" + cliente + "' AND upper(name) LIKE '%" + this.configuracion.getTarifa().toUpperCase() + "%' AND isactive = 'Y' LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				pricelist = rs.getString("m_pricelist_id");
			}
			
			rs = null;
			sql = "SELECT fin_paymentmethod_id FROM fin_paymentmethod WHERE ad_client_id = '" + cliente + "' AND upper(name) LIKE '%" + this.configuracion.getMetodopago().toUpperCase() + "%' AND isactive = 'Y' LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				metodopago = rs.getString("fin_paymentmethod_id");
			}
			
			rs = null;
			sql = "SELECT count(*) AS total FROM c_order WHERE upper(documentno) LIKE '" + nrodocumento + "' AND ad_client_id = '" + cliente + "' AND ad_org_id = '" + organizacion + "' AND issotrx = 'Y';";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				existedoc = rs.getInt("total") > 0 ? true : false;
			}
			
			if(cliente == null){
				mensaje = "No hay la Empresa en la Sucursal " + sucursal + " en Openbravo, comun�quese con el administrador";
			}else if(organizacion == null){
				mensaje = "No hay la Sucursal " + sucursal + " en Openbravo, verifique los datos";
			}else if(doctipe == null){
				mensaje = "No hay el Tipo de Documento en la Sucursal " + sucursal + " en Openbravo, comun�quese con el administrador";
			}else if(cbpartner == null){
				mensaje = "No hay el Cliente " + tercero + " en la Sucursal " + sucursal + " en Openbravo, verifique los datos o comun�quese con el administrador";
			}else if(cbpartnerdir == null){
				mensaje = "No tiene direcci�n el Cliente " + tercero + " en la Sucursal " + sucursal + " en Openbravo, verifique los datos o comun�quese con el administrador";
			}else if(paymentterm == null){
				mensaje = "No hay la Condici�n de pago en la Sucursal " + sucursal + " en Openbravo, comun�quese con el administrador";
			}else if(bodega == null){
				mensaje = "No hay una Bodega configurada en la Sucursal " + sucursal + " en Openbravo, comun�quese con el administrador";
			}else if(pricelist == null){
				mensaje = "No hay la Tarifa de venta en la Sucursal " + sucursal + " en Openbravo, comun�quese con el administrador";
			}else if(metodopago == null){
				mensaje = "No hay el Metodo de pago en la Sucursal " + sucursal + " en Openbravo, comun�quese con el administrador";
			}else if(existedoc){
				mensaje = "Ya existe un Pedido de Venta con este n�mero: " + nrodocumento + " en Openbravo, verifique los datos";
			}else{
				sql = "INSERT INTO c_order (c_order_id, ad_client_id, ad_org_id, isactive, "
						+ "created, createdby, updated, updatedby, issotrx, "
						+ "documentno, docstatus, docaction, processing, processed, "
						+ "c_doctype_id, c_doctypetarget_id, description, isdelivered, "
						+ "isinvoiced, isprinted, isselected, salesrep_id, "
						+ "dateordered, datepromised, dateprinted, dateacct, "
						+ "c_bpartner_id, billto_id, c_bpartner_location_id, poreference, "
						+ "isdiscountprinted, c_currency_id, paymentrule, c_paymentterm_id, "
						+ "invoicerule, deliveryrule, freightcostrule, freightamt, "
						+ "deliveryviarule, m_shipper_id, c_charge_id, chargeamt, "
						+ "priorityrule, totallines, grandtotal, m_warehouse_id, m_pricelist_id, "
						+ "istaxincluded, c_campaign_id, c_project_id, c_activity_id, posted, "
						+ "ad_user_id, copyfrom, dropship_bpartner_id, dropship_location_id, "
						+ "dropship_user_id, isselfservice, ad_orgtrx_id, user1_id, "
						+ "user2_id, deliverynotes, c_incoterms_id, incotermsdescription, generatetemplate, "
						+ "delivery_location_id, copyfrompo, fin_paymentmethod_id, fin_payment_priority_id, "
						+ "rm_pickfromshipment, rm_receivematerials, rm_createinvoice, c_return_reason_id, "
						+ "rm_addorphanline, a_asset_id, calculate_promotions, c_costcenter_id, convertquotation, "
						+ "c_reject_reason_id, validuntil, quotation_id, so_res_status, create_polines, iscashvat) "
						+ "VALUES (get_uuid(), '" + cliente + "', '" + organizacion + "', 'Y', "
						+ "now(), '100', now(), '100', 'Y', "
						+ "'" + nrodocumento + "', 'IP', 'CO', 'N', 'N', "
						+ "'" + doctipe + "', '" + doctipe + "', NULL, 'N', "
						+ "'N', 'N', 'N', NULL, "
						+ "'" + fecha + "', '" + fecha + "', NULL, '" + fecha + "', "
						+ "'" + cbpartner + "', '" + cbpartnerdir + "', '" + cbpartnerdir + "', NULL, "
						+ "'N', '100', '2', '" + paymentterm + "', "
						+ "'I', 'R', 'I', 0, "
						+ "'D', NULL, NULL, 0, "
						+ "'5', 0, 0, '" + bodega + "', '" + pricelist + "', "
						+ "'N', NULL, NULL, NULL, 'N', "
						+ "NULL, 'N', NULL, NULL, "
						+ "NULL, 'N', NULL, NULL, "
						+ "NULL, NULL, NULL, NULL, 'N', "
						+ "NULL, 'N', '" + metodopago + "', NULL, "
						+ "'N', 'N', 'N', NULL, "
						+ "'N', NULL, 'N', NULL, 'N', "
						+ "NULL, now(), NULL, NULL, 'N', 'N');";
				
				registro = statement.executeUpdate(sql);
				
				if(registro == 1){
					mensaje = "Pedido de Venta Guardado";
					connection.commit();
				}else{
					mensaje = "Pedido de Venta No Guardado";
					connection.rollback();
				}
			}
			
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			mensaje = "Pedido de Venta No Guardado, error: " + ex.getMessage();
			try {connection.rollback();} catch (SQLException e) {}
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return mensaje;
	}
	
	public String savePedidoLinea(String sucursal, String nrodocumento, String producto, int cantidad, Float preciounitario, String impuesto, Connection connection){
		ResultSet rs = null;
		Statement statement = null;
		String sql;
		
		int registro = 0;
		String cliente = null;
		String organizacion = null;
		String orden = null;
		String cbpartner = null;
		String cbpartnerdir = null;
		String nrolinea = null;
		String produc = null;
		String bodega = null;
		String ctax = null;
		
		String mensaje = "";
		
		try {
			statement = connection.createStatement();
			
			rs = null;
			sql = "SELECT ad_org_id, ad_client_id FROM ad_org WHERE upper(value) LIKE '%" + sucursal.toUpperCase() + "%' LIMIT 1;";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				organizacion = rs.getString("ad_org_id");
				cliente = rs.getString("ad_client_id");
			}
			
			rs = null;
			sql = "SELECT c_order_id, c_bpartner_id, c_bpartner_location_id, m_warehouse_id FROM c_order WHERE upper(documentno) LIKE '" + nrodocumento + "' AND ad_client_id = '" + cliente + "' AND ad_org_id = '" + organizacion + "' AND issotrx = 'Y';";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				orden = rs.getString("c_order_id");
				cbpartner = rs.getString("c_bpartner_id");
				cbpartnerdir = rs.getString("c_bpartner_location_id");
				bodega = rs.getString("m_warehouse_id");
			}
			
			rs = null;
			sql = "SELECT (COALESCE(max(line),0) + 10) AS linea FROM c_orderline WHERE c_order_id = '" + orden + "';";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				nrolinea = rs.getString("linea");
			}
			
			rs = null;
			sql = "SELECT m_product_id FROM m_product WHERE ad_client_id = '" + cliente + "' AND upper(value) LIKE '%" + producto + "%';";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				produc = rs.getString("m_product_id");
			}
			
			rs = null;
			sql = "SELECT c_tax_id FROM c_tax WHERE ad_client_id = '" + cliente + "' AND upper(name) = '" + impuesto.toUpperCase() + "';";
			rs = statement.executeQuery(sql);
			
			while(rs.next()){
				ctax = rs.getString("c_tax_id");
			}
			
			if(cliente == null){
				mensaje = "No hay la Empresa en la Sucursal " + sucursal + " en Openbravo, comun�quese con el administrador";
			}else if(organizacion == null){
				mensaje = "No hay la Sucursal " + sucursal + " en Openbravo, verifique los datos";
			}else if(orden == null){
				mensaje = "No hay un Pedido de Venta con este n�mero: " + nrodocumento + " en la sucursal " + sucursal + " en Openbravo, verifique los datos";
			}else if(produc == null){
				mensaje = "No hay el producto " + producto + " en la sucursal " + sucursal + " en Openbravo, verifique los datos";
			}else if(ctax == null){
				mensaje = "No hay el impuesto " + impuesto + " en la sucursal " + sucursal + " en Openbravo, verifique los datos";
			}else{
				Float totallinea = preciounitario * cantidad;
				
				sql = "INSERT INTO c_orderline (c_orderline_id, ad_client_id, ad_org_id, isactive, created, createdby, "
						+ "updated, updatedby, c_order_id, line, c_bpartner_id, c_bpartner_location_id, "
						+ "dateordered, datepromised, datedelivered, dateinvoiced, description, "
						+ "m_product_id, m_warehouse_id, directship, c_uom_id, qtyordered, qtyreserved, "
						+ "qtydelivered, qtyinvoiced, m_shipper_id, c_currency_id, pricelist, priceactual, pricelimit, linenetamt, "
						+ "discount, freightamt, c_charge_id, chargeamt, c_tax_id, "
						+ "s_resourceassignment_id, ref_orderline_id, m_attributesetinstance_id, isdescription, quantityorder, m_product_uom_id, "
						+ "m_offer_id, pricestd, cancelpricead, c_order_discount_id, iseditlinenetamt, taxbaseamt, "
						+ "m_inoutline_id, c_return_reason_id, gross_unit_price, line_gross_amount, grosspricelist, c_costcenter_id, grosspricestd, a_asset_id, m_warehouse_rule_id, user1_id, quotationline_id, user2_id, create_reservation, c_project_id, so_res_status, manage_reservation, manage_prereservation, explode, bom_parent_id, print_description, overdue_return_days, relate_orderline) "
						+ "VALUES (get_uuid(), '" + cliente + "', '" + organizacion + "', 'Y', now(), '100', "
						+ "now(), '100', '" + orden + "', '" + nrolinea + "', '" + cbpartner + "', '" + cbpartnerdir + "', "
						+ "now(), now(), NULL, NULL, NULL, "
						+ "'" + produc + "', '" + bodega + "', 'N', '100', '" + cantidad + "', 0, "
						+ "0, 0, NULL, '100', 0, '" + preciounitario + "', 0, '" + totallinea + "', "
						+ "0, 0, NULL, 0, '" + ctax + "', "
						+ "NULL, NULL, NULL, 'N', NULL, NULL, "
						+ "NULL, '" + preciounitario + "', 'N', NULL, 'N', '" + totallinea + "', "
						+ "NULL, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'N', 'N', 'N', NULL, 'N', NULL, 'N');";
				
				registro = statement.executeUpdate(sql);
				
				if(registro == 1){
					mensaje = "Linea de Pedido de Venta Guardado";
					connection.commit();
				}else{
					mensaje = "Linea de Pedido de Venta No Guardado";
					connection.rollback();
				}
			}
			
			rs.close();
		} catch (Exception ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			mensaje = "Linea de Pedido de Venta No Guardado, error: " + ex.getMessage();
			try {connection.rollback();} catch (SQLException e) {}
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}

		return mensaje;
	}
}
